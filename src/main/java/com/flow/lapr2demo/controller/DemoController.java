package com.flow.lapr2demo.controller;

import com.flow.lapr2demo.model.ListViewClient;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lapr2.framework.company.location.postaladdress.PostalAddress;
import lapr2.framework.company.location.postcode.PostCode;
import lapr2.framework.company.user.client.Client;
import lapr2.framework.company.user.client.ClientManager;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class DemoController implements Initializable, MapComponentInitializedListener {

	private static final String DATA_FILE_EXTENSION = "clients.dat";

	@FXML
	private ListView<ListViewClient> listViewClients;

	@FXML
	private GoogleMapView mapView;

	private GoogleMap map;
	private Stage stage;
	private HashMap<ListViewClient, List<Marker>> markers = new HashMap<>();
	private List<InfoWindow> infoWindows = new ArrayList<>();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mapView.addMapInializedListener(this);
	}

	@Override
	public void mapInitialized() {
		MapOptions mapOptions = new MapOptions();
		mapOptions.center(new LatLong(41.1601098, -8.6602373))
				.mapType(MapTypeIdEnum.ROADMAP)
				.overviewMapControl(false)
				.panControl(false)
				.rotateControl(false)
				.scaleControl(false)
				.streetViewControl(false)
				.zoomControl(false)
				.zoom(10);

		map = mapView.createMap(mapOptions);
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public void onOpenFile(ActionEvent actionEvent) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select the data file...");
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(DATA_FILE_EXTENSION, DATA_FILE_EXTENSION));

		File file = fileChooser.showOpenDialog(stage);
		if (file == null) return;

		ClientManager clientManager = new ClientManager(file.getAbsolutePath());

		if (!clientManager.load()) return;
		update(clientManager.getElements());
	}

	public void onQuit(ActionEvent actionEvent) {
		stage.close();
		System.exit(0);
	}

	public void onListClick(MouseEvent mouseEvent) {
		infoWindows.forEach(infoWindow -> infoWindow.close());
		infoWindows.clear();

		ListViewClient listViewClient = listViewClients.getSelectionModel().getSelectedItem();

		if (listViewClient == null) return;

		if (mouseEvent.getClickCount() == 2) {
			for (Marker marker : markers.get(listViewClient)) {
				InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
				infoWindowOptions.content(String.format("<h3>%s</h3>", listViewClient));

				InfoWindow infoWindow = new InfoWindow(infoWindowOptions);
				infoWindow.open(map, marker);

				infoWindows.add(infoWindow);
			}
		}
	}

	private void update(List<Client> clients) {
		List<ListViewClient> listViewClientList = new ArrayList<>();

		for (Client client : clients) {
			ListViewClient listViewClient = new ListViewClient(client);
			listViewClientList.add(listViewClient);

			List<Marker> listMarkers = new ArrayList<>();

			for (PostalAddress postalAddress : client.getPostalAddressList().getElements()) {
				PostCode postCode = postalAddress.getPostCode();
				LatLong latLong = new LatLong(postCode.getLatitude(), postCode.getLongitude());

				MarkerOptions markerOptions = new MarkerOptions();
				markerOptions.position(latLong);

				Marker marker = new Marker(markerOptions);

				map.addMarker(marker);

				listMarkers.add(marker);
			}

			markers.put(listViewClient, listMarkers);
		}

		listViewClients.setItems(FXCollections.observableList(listViewClientList));
	}
}
