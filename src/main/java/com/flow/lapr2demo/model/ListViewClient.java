package com.flow.lapr2demo.model;

import lapr2.framework.company.user.client.Client;

public class ListViewClient {

	private Client client;

	public ListViewClient(Client client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return String.format("%s (%s)", client.getName(), client.getTaxIdentificationNumber());
	}
}
