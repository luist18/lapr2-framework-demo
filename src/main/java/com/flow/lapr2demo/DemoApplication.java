package com.flow.lapr2demo;

import com.flow.lapr2demo.controller.DemoController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class DemoApplication extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Demo Application");
		primaryStage.setResizable(true);

		DemoController demoController = (DemoController) loadFXML(primaryStage, "/demo.fxml");
		demoController.setStage(primaryStage);
	}

	private Initializable loadFXML(Stage stage, String filePath) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(filePath));

		Parent root = loadParent(loader);

		assert root != null;

		Scene scene = new Scene(root);

		stage.setResizable(false);
		stage.setScene(scene);
		stage.sizeToScene();
		stage.centerOnScreen();
		stage.show();

		return loader.getController();
	}

	private Parent loadParent(FXMLLoader fxmlLoader) {
		Parent root = null;

		try {
			root = fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return root;
	}
}
